package com.hx.chapert1;

/**
 * 实型（浮点型）
 * 1.float  （4字节 32位）
 * 2.double （8字节 64位）
 * 常量：
 * 1.浮点常量默认为double，后面加f（F）代表float
 * 2.小数形式：3.14 2.66  指数形式：2.56e2
 */
public class FloatDemo {
    public static void main(String[] args) {
        float a=1.1f;
        System.out.println(a);
        double b=102;
        System.out.println(b);
        System.out.println(a*b);
    }
}
