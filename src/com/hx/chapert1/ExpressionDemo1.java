package com.hx.chapert1;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.IOException;
import java.sql.SQLOutput;
/**
 * 表达式： +  -  *  /  %（求余）
 *
 * 运算符类型要相同，结果为运算对象类型
 * 运算对象类型不同，则类型会自动转换[byte short char]同一级（转 int）-->int-->long-->float-->double
 */
import java.util.Scanner;

public class ExpressionDemo1 {
    public static void main(String args[]){
        Scanner ag=new Scanner(System.in);
                int age=ag.nextInt();
        if (age< 0) {
            System.out.println("不可能！");
        } else if (age>150) {
            System.out.println("是个乌龟！");
        } else {
            System.out.println("小女子芳龄 ：" + age +" ，嘻嘻嘻！");
        }

    }


   /* public static void main(String[] args) {
        int x = 4;
        int y = 1;
        if (x > 2) {
            if (y > 2)
                System.out.println(x + y);
                System.out.println("hello");
        } else
            System.out.println("x is " + x);
    }*/


   /* public static void main(String[] args) {
        int a=1,b=2 ;
         a=++b;
         b=a++;
        System.out.println(a+" "+b);
    }*/




    /*public static void main(String[] args) {
        int a=1,b=1;
        boolean f;
        f=a--==0&&b++>0;
        System.out.println(a+"  "+b+"   "+f);
        f=a--==0&&b++>0;
        System.out.println(a+"  "+b+"   "+f);
    }*/
    //短路：只计算前面一个的值






   /* public static void main(String[] args) throws IOException {
        Scanner scanner=new Scanner(System.in);
        System.out.println("输入成绩：");
        int s=scanner.nextInt();
        String r=s>=80?s>=90?"优秀":"良好":s>=60?"合格":"不合格";
        System.out.println(r);*/


       /* Scanner scanner=new Scanner(System.in);
        System.out.println("输入成绩：");
        int s=scanner.nextInt();
        String r=s>=90?"优秀":s>=80?"良好":s>=60?"合格":"不合格";
        System.out.println(r);*/

       /* Scanner scanner=new Scanner(System.in);
        System.out.println("输入成绩：");
        int s=scanner.nextInt();
        String r=s<60?"不合格":s>=80?s>=90?"优秀":"良好":"合格";
        System.out.println(r);*/

       /* Scanner scanner=new Scanner (System.in);
        System.out.println("输入成绩：");
        int s=scanner.nextInt();
        String result=s<60?"不合格":s<80&&s>=60?"合格":s>=80&s<90?"良好":"优秀";
        System.out.println(result);*/


       /* System.out.println("5+5="+5+5);
        System.out.println('*' + '\t' +'*');
        System.out.println("*" + '\t' +'*');
        System.out.println((int)'*');
        System.out.println((int)'\t');*/






       /* int a=6,b=3;
        boolean x=a>5&&((b=6)>4);
        System.out.println(x+"   "+a+"   "+b);
        short c=128;
        int d=100;
        System.out.println(c>d);*/



       /* char x=(char)System.in.read();
        System.out.println("输出的是不是字母："+(x>=65&&x<=90||x>=95&&x>=122));*/

       /* char x=(char)System.in.read();
        System.out.println("输出的是不是大写字母："+(x>=65&&x<=90));
        System.out.println("输出的是不是小写字母："+(x>=97&&x<=122));*/


       /* Scanner scanner=new Scanner (System.in);
        System.out.println("请输入一个4位整数：");
        int num=scanner.nextInt();
        int gw=num%10;
        int sw=num%100;
            sw=sw/10;
        int bw=(num/100)%10;
        int qw=(num/1000)%10;
        System.out.println("个位是："+gw);
        System.out.println("十位是："+sw);
        System.out.println("百位是："+bw);
        System.out.println("千位是："+qw);*/
    }

