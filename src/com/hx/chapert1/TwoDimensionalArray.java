package com.hx.chapert1;

import java.util.Scanner;

public class TwoDimensionalArray {
    public static void main(String[] args) {


        //杨辉三角（行=列）
        System.out.println("请输入一个整数：");//输入行数
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = n;
        int[][] a = new int[n][m];//创建数组

        //判断条件
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j <= i; j++) {
                if (j == 0 || i == j) {
                    a[i][j] = 1;
                } else {
                    a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
                }
            }
        }
        //输出数组
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j <= i; j++)
                System.out.print(a[i][j] + " ");
            System.out.println();
        }
    /*public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入一个数：");
        int n=scanner.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = 1;
            }
        }
        for (int i = 2; i < a.length; i++) {
            for (int j = 1; j < i; j++) {
                a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }*/
    }
}