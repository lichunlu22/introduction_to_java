package com.hx.chapert1;

import java.util.Scanner;

public class Exercise {
    //1.从控制台随机录入一个四位数，求四位数每位数字之和。例如：输入1234，得到每位数之和是10（提示，使用取商’/’和取余’%’配合使用）
    /*public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入一个四位数：");
        int sz=scanner.nextInt();
        int gw,sw,bw,qw;
        gw=sz%10;
        sw=sz/10%10;
        bw=sz/100%10;
        qw=sz/1000;
        System.out.println(gw+sw+bw+qw);
    }*/

    /*2.实行新的抽奖规则：会员号的百位数字等于产生的随机数字即为幸运会员。基于第2章阶段3，实现：
    a)	从键盘接受会员号，
    b)	生成随机数
    c)	算出会员号在百位的数字的号码
    d)	使用if  else  实现幸运抽奖*/
   /* public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入会员号：");
        int hy=scanner.nextInt();
        int bw=hy/10%10;
        int sjs=(int)(Math.random()*10);
        if (bw==sjs)
            System.out.println("恭喜你成为幸运会员！");
        else
            System.out.println("你不是幸运会员！");

    }*/

//3.会员根据不同的积分，享受不同的折扣，使用多重if选择结构实现
/*public static void main(String[] args) {
    Scanner scanner=new Scanner(System.in);
    System.out.print("请输入会员积分：");
    int x=scanner.nextInt();
    if (x<2000)
        System.out.println("该会员享受的折扣是：0.9");
    if (x>=2000&&x<4000)
        System.out.println("该会员享受的折扣是：0.8");
    if (x>=4000&&x<8000)
        System.out.println("该会员享受的折扣是：0.7");
    if (x>=8000)
        System.out.println("该会员享受的折扣是：0.6");
}*/

//4.输入年份和月份，得到该月的天数。
/*public static void main(String[] args) {
    Scanner scanner=new Scanner(System.in);
    System.out.print("请输入年份，月份：");
    int y=scanner.nextInt(),
        m=scanner.nextInt(),
        day;
        switch (m){
            case 2:
                if (y%100!=0&&y%4==0||y%400==0)
                    day=29;
                else
                    day=28;
                break;
            case 4: case 6: case 9: case 11:
                day=30;
                break;
            default:
                day=31;
        }

    System.out.println(day);

}*/

//5.输入三个正数，判断是否能够构成三角形。
/*public static void main(String[] args) {
    Scanner scanner=new Scanner(System.in);
    System.out.println("输入三个正数：");
    int a=scanner.nextInt(),
        b=scanner.nextInt(),
        c=scanner.nextInt();
    if (a+b>c&&a+c>b&&b+c>a)
        System.out.println("可以构成三角形！");
     else
        System.out.println("不能构成三角形");
}*/
//6. 2016年培养学员8万人，每年增长25%，请问按此增长速度，到哪一年培训学员人数将达到20万人？
/*public static void main(String[] args) {
    int year=2016;
    double students=80000;
    while (students<200000){
        students=students*(1+0.25);
                year++;
    }
    System.out.println(year);
}*/

//7.输出长方形。
/*public static void main(String[] args) {
    Scanner scanner=new Scanner(System.in);
    System.out.println("输入长宽：");
    int a=scanner.nextInt(),
        b=scanner.nextInt();
    for (int i=0;i<b;i++){
        for (int j=0;j<a;j++)
            System.out.print("*");
         System.out.println();}*/

//8.空心长方形。
/*public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("输入长宽：");
    int a = scanner.nextInt(),
        b = scanner.nextInt();
    for ( int i = 0; i < b; i++) {
        for (int j=0;j<a;j++)
            if (j==0||j==a-1||i==0||i==b-1)
                System.out.print("*");
            else
                System.out.print(" ");
        System.out.println();
    }

}*/


//9.输出三角形
/*public static void main(String[] args) {
    Scanner scanner=new Scanner(System.in);
    int n=scanner.nextInt();
    for (int i=1;i<n;i++){
        for (int j=1;j<n-i;j++)
            System.out.print(" ");
        for (int j=1;j<2*i;j++)
            System.out.print(i);
        System.out.println();
    }
}*/

//10.从键盘输入一个0~99999之间的任意数，判断输入的数是几位数？
/*public static void main(String[] args) {
    Scanner scanner=new Scanner(System.in);
    int a=scanner.nextInt();
    int b=0;
    if (a<100000&&a>0) {
        while (a > 0) {
            a = a / 10;
            b++;
        }
        System.out.println(b);
    }else
        System.out.println("超出范围");

}*/


//11.编写程序解决“百钱买百鸡”问题。公鸡五钱一只，母鸡三钱一只，小鸡一钱三只，现有百钱欲买百鸡，共有多少种买法？
public static void main(String[] args) {
    double x,y,z;
    for (x=0;x<20;x++){
        for (y=0;y<=33;y++){
            z=100-x-y;
            if (x+y+z==100&&5*x+3*y+z/3==100)
                System.out.println("公鸡"+x+"只"+"  "+"母鸡"+y+"只"+"  "+"小鸡"+z+"只");
        }
    }
}
//12.判断是不是回文数字
   /* public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入整数：");
        int zs=scanner.nextInt();
        int cunt=0;
        for ()
            cunt=zs/10的0
        while (zs > 0) {
            zs = zs / 10;
            cunt++;
        }

    }*/

   /* public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int x=scanner.nextInt();
        int w=x,y=0;
        while (x>0){
            y=y*10+x%10;
            x=x/10;
        }
        if (y==w)
        System.out.println("是回文数");
        else
            System.out.println("不是回文数");

    }
*/




}//第一个大括号.
