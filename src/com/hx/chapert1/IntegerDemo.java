package com.hx.chapert1;
/**
 * 整型：
 * 1.byte （1字节 8位）
 * 2.short （2字节 16位）
 * 3.int （4字节 32位）
 * 4.long （8字节 64位）
 * 常量：
 * 1.二进制 （0b）
 * 2.八进制（0）
 * 3.十六进制 （0x）
 * 4.十进制 （什么都不加）
 *
 * 整数常量默认为int，后面加l（L）代表long
 */
public class IntegerDemo {
    public static void main(String[] args) {
        byte a=100;
        System.out.println("a="+a);
        byte b=101;
        System.out.println("b="+b);
        System.out.println(a+b);
        System.out.println(a*b);
        if (a>b){
            System.out.println(a+b);

        }
        else {
            System.out.println(b-a);
        }

    }

}
