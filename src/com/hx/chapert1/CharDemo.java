package com.hx.chapert1;

/**
 * 字符型
 * 1.char 2B
 * 常量：
 * 1.‘字符’ ‘a’  ‘0’
 * 2.转义字符：‘\n’ '\b' '\'' '\"' '\\'
 * 运算：
 * 用编码值运算
 * ASCII:
 * 1.0对应的是48
 * 2.A对应的是65
 * 3.a对应的是97
 */
public class CharDemo {
    public static void main(String[] args){
        char a='云';
        char b='a';
        char f='\b';
        char g='\\';
        String str2 = 3.5f + "12";
        System.out.println(""+a+b+f+g);
        System.out.println((int)'云');
        System.out.println(b);
        System.out .println(3+4+"Hello!");
        System.out.println("Hello!"+3+4);
        System.out.println("Hello"+'a'+1);
        System.out.println('a'+1+"Hello!");
        System.out.println(str2);
    }
}
