package com.hx.chapert1;

import java.util.Random;

public class PersonalMethod {
    public static void chooseSort(int[] sort1) {
        for (int i = 0; i < sort1.length - 1; i++) {
            int k = i;
            for (int j = i + 1; j < sort1.length; j++) {
                if (sort1[j] < sort1[k]) k = j;
            }
            if (i != k) {
                int l = sort1[i];
                sort1[i] = sort1[k];
                sort1[k] = l;
            }
        }
    }//选择排序

    public static void bubblingSort(int[] sort) {
        int a;
        for (int i = 0; i < sort.length - 1; i++) {
            for (int j = 0; j < sort.length - 1 - i; j++) {
                if (sort[j] > sort[j + 1]) {
                    a = sort[j];
                    sort[j] = sort[j + 1];
                    sort[j + 1] = a;
                }
            }
        }
    }//冒泡排序

    public static void SuiJiShuZu(int[] avg, Random random) {
        for (int i = 0; i < avg.length; i++)
            avg[i] = random.nextInt(50);
    }//随机数组

    public static void output(int[] avg) {
        for (int i = 0; i < avg.length; i++)
            System.out.print(avg[i] + " ");
    }//输出随机数组

}