package com.hx.collection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class ListDemo1 {
    public static void main(String[] args) {
        ArrayList<Date> dates=new ArrayList<>();
        dates.add(new Date());
        dates.add(new Date(System.currentTimeMillis() +24*60*60*1000));
        SimpleDateFormat sdf=new SimpleDateFormat( "yyyy--MM--dd HH:mm:ss");
            for (int i=0;i<dates.size();i++){
                Date date1=dates.get(i);
                System.out.println(sdf.format(date1));

            }
                System.out.println("========");
            Iterator<Date> iterator=dates.iterator();
            while(iterator.hasNext()){
                Date date= iterator.next();
                System.out.println(sdf.format(date));
            }
        }






}
