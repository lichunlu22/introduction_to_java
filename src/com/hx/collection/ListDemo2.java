package com.hx.collection;

import java.util.ArrayList;
import java.util.List;

public class ListDemo2 {
    public static void main(String[] args) {
        List<User> users=new ArrayList<>();
        User u1=new User("张三",19);
        //users.add(u1);
        users.add(new User("张三",20));
        users.add(new User("李四",21));
        users.add(new User("王五",21));

       /* for (User u:users){
            if (u.getName().equals("张三")){
                users.remove(u);
            }
        }*/

       for (User u:users){
          System.out.println(u);

        }
        System.out.println("=================");
        User u2=new User("张三",20);
        users.add(u2);
        System.out.println(users.contains(u2));
        System.out.println(users.indexOf(u2));
        System.out.println(users.lastIndexOf(u2));


        List<User> users1=new ArrayList<>();
        users1.add(new User("李四",21));
        users1.add(new User("王五",21));
        users.addAll(users1);
        System.out.println("------------");
        for (User u:users){
            System.out.println(u);

        }

        System.out.println(users.contains(users1));



    }
}
