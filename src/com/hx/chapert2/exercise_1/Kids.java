package com.hx.chapert2.exercise_1;

public class Kids extends Mankind {
    private int yearsOld;

    public Kids(){
        super();
        System.out.println("子类构造方法");
    }

    public Kids(int sex,int yearsOld){
        super(sex,0);
        this.yearsOld=yearsOld;
    }

    @Override
    public void manOrWorman() {
        System.out.println("kids");
        //super.manOrWorman();
    }

    public void printAge(){
        System.out.println(yearsOld);
    }


    public static void main(String[] args) {
        Kids someKids = new Kids();
        /*someKids.printAge();
        someKids.employeed();
        someKids.manOrWorman();*/
       /* Mankind mk=someKids;
        mk.manOrWorman();
        Mankind mks=new Mankind(0,3000);
        mks.manOrWorman();*/

    }
}
