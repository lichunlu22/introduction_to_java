package com.hx.chapert2;

public class Max {


    public int max(int a,int b){
        if (a>b)
            return a;
        else
            return b;
    }

    public double max(double a,double b){
        if (a>b)
            return a;
        else
            return b;
    }

    public double max(double a,double b,double c){
        if (a>b&&a>c)
            return a;
        else if (b>a&&b>c)
            return b;
        else
            return c;
    }

    public static  int max(int ...x){
        int max=x[0];
        for (int i=0;i<x.length;i++)
            if (x[i]>max) max=x[i];
        return max;
    }

    public static void main(String[] args) {

        System.out.println(max(1,6,9,7,5,8,6,4,5,6,5,8,5,8,5,5,125,85,95,65,45));

       /* System.out.println(max.max(1,2));
        System.out.println(max.max(2.0,3.9));
        System.out.println(max.max(3.5,6.2,5.9));*/
    }
}
