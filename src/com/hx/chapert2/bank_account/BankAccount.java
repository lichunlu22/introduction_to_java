package com.hx.chapert2.bank_account;

public class BankAccount {
    private String id;
    private long password;
    private double deposit;//存款
    private double interestrate;//利率
    public static double minimumbalance=10;//最小余额

    public BankAccount(long password, double deposit, double interestrate) {

        this.password = password;
        this.deposit = deposit;
        this.interestrate = interestrate;
    }

    public BankAccount(String id) {
        this.id = id;
    }

    public void display(){
        System.out.println("账号："+id+"\n密码："+password+"\n存款余额："+deposit+
                "\n利率："+interestrate+"\n最小余额："+minimumbalance);
    }

    public static void main(String[] args) {
        BankAccount id1=new BankAccount(123456,10000,0.35);
        id1.display();
    }

}
