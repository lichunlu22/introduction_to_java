package com.hx.chapert2;

/**
 * 人类
 * @author lcl by 2018/12/24
 * 版本
 */
public class Person {
    //属性
    //修饰符=》public private protected 默认
    //【修饰符】 【static】【final】 类型 成员名；
    private String name;//姓名  私有的  只能在本类中被访问
    public int age;//公共    任何地方都可被访问
    protected String phone;//保护   同包及子类可访问
    String address;//默认   同包中可被访问
}
