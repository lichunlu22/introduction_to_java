package com.hx.chapert2;

public class Teacher {
    private String name;
    private String major;
    private String course;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void display(){
        System.out.println("姓名:"+name+"\n"+"专业："+major+"\n"+"教授的课程："+course+"\n"+"教龄："+age);

    }
}
