package com.hx.chapert2;

import java.security.PublicKey;

public class Point {
    private double x;
    private double y;
    private double z;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double point(){
        return x*x+y*y+z*z;
    }

    public static void main(String[] args) {
        Point p1=new Point(10,10,10);
        Point p2=new Point(20,20,20);
        Point p3=new Point(2,3,6);

        System.out.println("这个点到原点的距离的平方是："+p1.point());
        System.out.println("这个点到原点的距离的平方是："+p2.point());
        System.out.println("这个点到原点的距离的平方是："+p3.point());

    }
}
