package com.hx.chapert2;

public class Person1 {
    private String name;
    private int age;
    private int sex;

    public Person1(int age){
        this.age=age;
    }
    public Person1(String name){
        this.name=name;
    }

    public Person1(String name, int age, int sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public void study(){
        System.out.println("studying");
    }
    public void showAge(){
        System.out.println("年龄："+age);
    }
    public void addAge(){
        System.out.println(age+=2);
    }
}
