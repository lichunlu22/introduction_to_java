package com.hx.chapert2.lcl;

public class Test55 {
    public static void main(String[] args) {
        Person a=new Person();
        System.out.println(a.getInfo());
        System.out.println("================");
        Student b=new Student();
        System.out.println(b.getInfo());
        System.out.println("================");
        Graduate c=new Graduate();
        System.out.println(c.getInfo());
        System.out.println("================");
        Person d=new Student();
        System.out.println(d.getInfo());
        System.out.println("================");
        Person e=new Graduate();
        System.out.println(e.getInfo());
        System.out.println("================");
        Student f=new Graduate();
        System.out.println(f.getInfo());
    }
}
