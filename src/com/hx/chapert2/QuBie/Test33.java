package com.hx.chapert2.QuBie;

public class Test33 {
    public static void main(String[] args) {
        Sub sub=new Sub();
        sub.display();

        Base sub1=new Base();
        sub1.display();

        Base base=new Sub();
        base.display();

        System.out.println("=========================");

        Sub s = new Sub();
        System.out.println(s.count);
        s.display();

        System.out.println("====================");

        Base b = new Sub();
        System.out.println(b == s);
        System.out.println(b.count);
        b.display();

    }
}
