package com.hx.chapert2.InterFace;

public class Test {
    public static void main(String[] args) {
        Test test=new Test();
        Man man=new Man();
        test.m1(man);
        test.m2(man);
        test.m3(man);
    }

    public String m1(Runner f) { f.run();return "100"; }
    public void  m2(Swimmer s) {s.swim();}
    public void  m3(Creator a) {a.eat();}
}
