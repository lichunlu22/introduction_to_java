package com.hx.chapert2.InterFace.jiekou;

public class ComparableRectangle implements CompareObject {
    private int length;
    private int width;
    private int mianji;

    public ComparableRectangle(int a, int b) {
        this.length = a;
        this.width = b;
        mianji=a*b;
    }


    @Override
    public int CompareObject(Object o) {
        ComparableRectangle other=(ComparableRectangle)o;
        return this.mianji-other.mianji;
    }
}
