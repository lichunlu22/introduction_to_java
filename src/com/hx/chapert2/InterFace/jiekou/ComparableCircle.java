package com.hx.chapert2.InterFace.jiekou;

public class ComparableCircle  implements CompareObject {

    private int r;


    public ComparableCircle() {
    }

    public ComparableCircle(int r) {
        this.r = r;
    }

    @Override
    public int CompareObject(Object o) {
        ComparableCircle abc=(ComparableCircle) o;
        return this.r-abc.r;
    }
}
