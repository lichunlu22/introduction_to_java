package com.hx.chapert2.InterFace.jiekou;

public class Students implements CompareObject {
    private String name;
    private int age;

    public Students() {
    }

    public Students(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int CompareObject(Object o) {
        Students other=(Students) o;
        return this.age-other.age;
    }
}
