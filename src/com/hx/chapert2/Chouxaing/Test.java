package com.hx.chapert2.Chouxaing;

public class Test {
    public void showDist(Vehicle vehicle){
        System.out.println(vehicle.calcFueLefficiency());
        System.out.println(vehicle.calcTripDistance());
    }

    public static void main(String[] args) {
        Test test=new Test();
        Truck truck=new Truck();
        test.showDist(truck);

        RiverBarge riverBarge=new RiverBarge();
        test.showDist(riverBarge);
    }
}
