package com.hx.chapert2.Chouxaing;

public abstract class Template {
    public long getTime(){
        long startTime=System.currentTimeMillis();
        code();
        long endTime=System.currentTimeMillis();
        return endTime-startTime;
    }
    public abstract void code();
}
class TemplateTest extends Template{
    @Override
    public void code() {
        int s=0;
        for (int i=1;i<10000;i++){
            s=s+i;
            System.out.println(s);
        }
    }
    public static void main(String[] args) {
        TemplateTest tt=new TemplateTest();
        System.out.println(tt.getTime());
    }
}
