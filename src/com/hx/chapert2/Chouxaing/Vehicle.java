package com.hx.chapert2.Chouxaing;

public abstract class Vehicle {
    public abstract double calcFueLefficiency();
    public abstract double calcTripDistance();

}
class Truck extends Vehicle{
    public double calcFueLefficiency(){
       return 100;
    }
    public double calcTripDistance(){
        return 1000;
    }
}
class RiverBarge extends Vehicle{
    public double calcFueLefficiency(){
        return 200;
    }
    public double calcTripDistance(){
        return 2000;
    }
}
