package com.hx.chapert2.singleton;

public class SingLeton {

    private static SingLeton lcl;

    {
        System.out.println("11111");
    }

    static {//类加载的时候
        System.out.println("22222");
    }

    private SingLeton() {
        System.out.println("3333333333");
    }

    public static SingLeton getInstance(){
        if (lcl==null)
            lcl=new SingLeton();
        return lcl;
    }

    public void sayhello(){
        System.out.println("别说话");
    }
}
