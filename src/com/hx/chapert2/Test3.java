package com.hx.chapert2;
/*class DataSwap{
    public int a;
    public int b;
}*/

public class Test3 {
  /*  public static void swap(DataSwap ds){
        int tmp = ds.a;
        ds.a = ds.b;
        ds.b = tmp;
        System.out.println("swap方法里，a Field的值是"
                + ds.a + "；b Field的值是" + ds.b);
    }
    public static void main(String[] args) {
        DataSwap ds = new DataSwap();
        ds.a = 6;
        ds.b = 9;
        swap(ds);
        System.out.println("交换结束后，a Field的值是"
                + ds.a + "；b Field的值是" + ds.b);
    }
*/


    public static void swap(int c , int d){
        int tmp = c;
        c = d;
        d = tmp;
        System.out.println("swap方法里，a的值是"
                + c + "；b的值是" + d);
    }
    public static void main(String[] args) {
        int a = 6;
        int b = 9;
        swap(a , b);
        System.out.println("交换结束后，变量a的值是"
                + a + "；变量b的值是" + b);
    }
}
