package com.hx.chapert2.inherit;

public class Person {
    protected String name;
    private int age;
    private String sex;

    public void eat(){
        System.out.println(name+"吃饭了");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
