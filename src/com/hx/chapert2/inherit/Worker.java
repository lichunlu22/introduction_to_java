package com.hx.chapert2.inherit;

public class Worker extends Person {

    private double salary;


    public void work(){
        System.out.println(name+"工作中");
    }


    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
