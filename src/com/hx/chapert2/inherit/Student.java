package com.hx.chapert2.inherit;

public class Student extends Person {
    private String sno;//学号

    public void study(){
        System.out.println(name+"学习中");
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }
}
