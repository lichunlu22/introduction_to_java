package com.hx.chapert2;

public class Circle1 {
    public static  final  double PI=3.1415926;

    private double radius;

    public Circle1(double radius){
        this.radius=radius;
    }

    public Circle1(){
    }

    public static double getPI() {
        return PI;
    }

    public double getRadius() {
        return radius;
    }


    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double area(){
        return PI*radius*radius;
    }

    public static void main(String[] args) {
        Circle1 c1=new Circle1();
        c1.setRadius(10);
        System.out.println(c1.area());

        Circle1 c2=new Circle1(5);
        System.out.println(c2.area());
    }
}
