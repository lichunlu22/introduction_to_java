package com.hx.week4_2.lichunlu4_2_3;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ZhengZe {
    public static void main(String[] args) {

        //匹配包含字母，数字和特殊符号的任意两种组合
        String pwd = "^(?![0-9]+$)(?![a-zA-Z]+$)|(?!\\W+$)[0-9A-Za-z\\W]{6,20}$";
        System.out.println("++++123456pppp".matches(pwd));

        //把叠词替换成#
        String s1 = "zhangsanQQQQQQQQQQQQlisiUUUUUUUUUUwangwu";
        s1 = s1.replaceAll("(.)\\1", "#");
        System.out.println(s1);

        //把叠词变成一个，把多个q变成一个Q
        s1 = s1.replaceAll("(.)\\1+", "$1");
        System.out.println(s1);

        //替换
       /* Scanner scanner = new Scanner(System.in);
        String tel = scanner.next();
        if (tel.length() == 11) {
            tel = tel.replaceAll("(\\d{3})(\\d{4})(\\d{4})", "$1****$3");
            System.out.println(tel);
        } else
            System.out.println("不符合规则，请重新输入：");
*/
        System.out.println("===========================");

        //切割  就是String的split的方法
        String str="zhangsan        xiaoqian                               wangwu   zhouqi";
        String[] strs=str.split(" +");
        for (String string:strs){
            System.out.println(string);
        }

        System.out.println("===============================");

        String sst="da  jia  hao ,wo shi zha zha hui";
        String reg1="\\b[a-zA-Z]{3}\\b";
        Pattern pattern=Pattern.compile(reg1);
        Matcher matcher= pattern.matcher(sst);
        while(matcher.find()){
            System.out.print(" "+matcher.group());
        }
        System.out.println("=======================================");
        String kkk="我我...我...我我...我喜....喜喜喜....喜喜...喜欢欢..欢欢..欢...欢亮亮亮...亮...亮哥";//字符串是不可改变的！！！！！！！！！！！！！！！！！！
         kkk=kkk.replaceAll("\\.+","");
         kkk=kkk.replaceAll("(.)\\1+","$1");
        System.out.println(kkk);

        String tel="151987616762";
        String rgg="^1[3,5,7,8][0-9]{9}$";
        boolean b=tel.matches(rgg);
        System.out.println(b);


        String qqq="dsaaaaaafdesfsdgsfsesegdrhyik15198716762yuolo,mhvgbxdxvsasefesdgeh18587330906etewwsfsdgdfhgdsgsgdxgxdg";
//        String www=

    }
}
