package com.hx.week4_2.lichunlu4_2_3;

public class StringBuffer1 {

    public static void main(String[] args) {
        String text = "";
        long startTime = 0L;
        long endTime = 0L;
        StringBuffer buffer = new StringBuffer("");

        StringBuilder builder = new StringBuilder("");

        startTime = System.currentTimeMillis();//获取当前系统时间

        for(int i = 0;i<20000;i++){
            buffer.append(String.valueOf(i));
        }

        //=========================================
        endTime = System.currentTimeMillis();
        System.out.println("StringBuffer的执行时间："+(endTime-startTime));
        startTime = System.currentTimeMillis();
        for(int i = 0;i<20000;i++){
            builder.append(String.valueOf(i));//把i转成字符串然后拼接
        }

        //==========================================
        endTime = System.currentTimeMillis();
        System.out.println("StringBuilder的执行时间："+(endTime-startTime));
        startTime = System.currentTimeMillis();
        for(int i = 0;i<20000;i++){
            text = text + i;
        }
        //==========================================
        endTime = System.currentTimeMillis();
        System.out.println("String的执行时间："+(endTime-startTime));

    }
}
