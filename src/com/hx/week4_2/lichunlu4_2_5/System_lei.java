package com.hx.week4_2.lichunlu4_2_5;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

public class System_lei {
    public static void main(String[] args) {
        //数组复制
        int []a={1,2,3,4,5,6,8};
        int []b=new int[15];
        System.arraycopy(a,1,b,5,6);
        for (int i=0;i<b.length;i++){
            System.out.print(b[i]+" ");
        }

        System.out.println();
        System.out.println("============================");

        //返回计算机当前时间
        long start=System.currentTimeMillis();
        System.out.println(start);
        for (int i=0;i<10000000;i++){
            int j=0;
        }
        long end=System.currentTimeMillis();
        System.out.println(end);
        long time=end-start;
        System.out.println(time);


        System.out.println("======================================");

        //getProperty方法：使用该方法可以获得很多系统级的参数以及对应的值
        String s=System.getProperty("java.version");
        System.out.println(s);
        String s1=System.getProperty("os.name");
        System.out.println(s1);
        String s2=System.getProperty("os.version");
        System.out.println(s2);
        String s3=System.getProperty("java.home");
        System.out.println(s3);

        System.out.println("==========================");

        //java 数组排序 Arrays
        //1.
        int[]number={5,30,56,23,0,2,60};
        Arrays.sort(number);
        for (int i=0;i<number.length;i++){
            System.out.print(number[i]+" ");
        }

        //2.
        System.out.println();
        String str="wangshiqiwangyinuo";
        char[] aaa=str.toCharArray();
        Arrays.sort(aaa);
        for (char c:aaa) {
        }
        System.out.println(aaa);


        System.out.println();
        System.out.println("==========================");

        //equals()
        int[]c={2,0,2,4,5};
        int[]d={2,0,2,4,5};
        System.out.println(c.equals(d));//false
        System.out.println(Arrays.equals(c,d));//true

        System.out.println("===================");
        //Integer类作为int的包装类，能存储的最大整型值为2^31−1
        // BigInteger类的数字范围较Integer类的数字范围要大得多，可以支持任意精度的整数。
        //1.
        double oo=7.35;
        double o=2.2;
        System.out.println(oo-o);

        //一般的Float类和Double类可以用来做科学计算或工程计算，
        // 但在商业计算中，要求数字精度比较高，故用到java.math.BigDecimal类。
        // BigDecimal类支持任何精度的定点数。
        BigDecimal bigDecimal=new BigDecimal(7.35);
        BigDecimal bigDecimal1=new BigDecimal(2.2);
        BigDecimal mmm=bigDecimal.subtract(bigDecimal1);
        mmm=mmm.setScale(3,BigDecimal.ROUND_UP);
        System.out.println(mmm);

        System.out.println("++++++++++++++++++++++++++++++++++++++++");
        int[] ff={6,5,4,6,45,87,8,4,5,8};
        for (int i=0;i<ff.length;i++){
            for (int j=0;j<ff.length-1-i;j++){
                if (ff[j]>ff[j+1]){
                    int temp=ff[j];
                    ff[j]=ff[j +1];
                    ff[j+1]=temp;
                }
            }
        }
        for (int k=0;k<ff.length;k++){
            System.out.print(ff[k]+" ");
        }

    }
}
