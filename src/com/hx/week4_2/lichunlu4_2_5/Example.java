package com.hx.week4_2.lichunlu4_2_5;

public class Example {
    public static int num=0;
    String name;
    static {
        System.out.println(num+":codaA");
    }
    {
        System.out.println(num+":codaB");
        num++;
    }

    public Example(String name){
        System.out.println(num+":构造器:"+name);
        this.name=name;
        num++;
    }

    public static void main(String[] args) {
        Example people=new Example("tom");
        Example people2=new Example("jim");
    }
}
