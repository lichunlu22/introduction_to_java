package com.hx.week4_2.lichunlu4_2_1;

public class Trim {
    public static void main(String[] args) {
        String s="   116611236544    11  55 11  55   11 55  ";
        String s1=trim1(s);
        System.out.println(s1);
        System.out.println(fanzhuan(s,5,11));
        System.out.println(findCount(s,"11"));
        System.out.println(maxSameSub("abcwerthelloyuiodef","cvhellobnm"));
    }
    /**
     * 获取两个字符串中最大相同子串。比如：
     *    str1 = "abcwerthelloyuiodef“;str2 = "cvhellobnm"
     *    提示：将短的那个串进行长度依次递减的子串与较长
     *    的串比较
     */
    public static String maxSameSub(String str1,String str2){
        if (str1.length()<str2.length()){
            String t=str1;str1=str2;str2=t;
        }
        int maxLen=str2.length();
        while(maxLen>0){
            for (int i=0;i+maxLen<str2.length();i++){
                String sub=str2.substring(i,i+maxLen);
                if (str1.indexOf(sub)>=0) return sub;
            }
            maxLen--;
        }
        return "";
    }

    /***
     * 查询出现次数
     * @return
     */
    public static int findCount(String str,String sub){
        int c=0,start=str.indexOf(sub,0);//第一次找到sub返回的下标
        while(start>=0){//找不到就返回-1，结束循环
            c++;
            start=str.indexOf(sub,start+sub.length());//从找到的位置加上本身长度再寻找
        }
        return c;//返回次数
    }

    /***
     * 去掉一个字符串首尾空格
     * @param x
     * @return
     */
    public static String  trim1(String x){


        int start = 0,end=0;

        char [] a=x.toCharArray();
        //找到前面第一个不是空格的下标
        for (int i=0;i<a.length;i++) {
            if (a[i] != ' ') {
                start = i;
                break;
            }
        }
        //找到从后面找的第一个不是空的下标
        for (int j=a.length-1;j>=0;j--)   {
            if (a[j]!=' '){
                end=j;
                break;
            }
        }

        return x.substring(start,end+1);

    }

    /***
     *
     * 反转字符串中指定位置
     */

    public static String fanzhuan(String x,int start,int end){
        char[] a=x.toCharArray();//把字符串转成字符串数组
        for (int i=start,j=end;i<j;i++,j--){
            char temp=a[i];
            a[i]=a[j];
            a[j]=temp;
        }

        return new String(a);
    }


}
