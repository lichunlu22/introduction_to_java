package com.hx.chapert_6_jdk.mydate;

import java.util.Objects;

public class Mydate {
    private long y;
    private int m;
    private int d;

    public long getY() {
        return y;
    }

    public void setY(long y) {
        this.y = y;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public Mydate(long y, int m, int d) {
        this.y = y;
        this.m = m;
        this.d = d;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;//判断类型
        Mydate mydate = (Mydate) o;
        return y == mydate.y &&
                m == mydate.m &&
                d == mydate.d;
    }

    @Override
    public int hashCode() {
        return Objects.hash(y, m, d);
    }

    public static void main(String[] args) {
        /*Mydate mydate1=new Mydate(2018,12,11);
        Mydate mydate2=new Mydate(2018,12,11);
        System.out.println(mydate1.equals(mydate2));*/

        Mydate m1 = new Mydate(14, 3, 1976);
        Mydate m2 = new Mydate(14, 3, 1976);

        if (m1 == m2) {
            System.out.println("m1==m2");
        } else {
            System.out.println("m1!=m2"); //m1 != m2,比较地址
        }

        if (m1.equals(m2)) {
            System.out.println("m1 is equal to m2");
            // m1 is equal to m2，equals重写以后比较值相不相等
        } else {
            System.out.println("m1 is not equal to m2");

        }

    }
}
