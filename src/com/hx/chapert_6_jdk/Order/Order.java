package com.hx.chapert_6_jdk.Order;

import java.util.Objects;

public class Order {
    private int orderld;
    private String OrderName;

    public int getOrderld() {
        return orderld;
    }

    public void setOrderld(int orderld) {
        this.orderld = orderld;
    }

    public String getOrderName() {
        return OrderName;
    }

    public void setOrderName(String orderName) {
        this.OrderName = orderName;
    }

    public Order(int orderld, String orderName) {
        this.orderld = orderld;
        OrderName = orderName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderld == order.orderld &&
                Objects.equals(OrderName, order.OrderName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderld, OrderName);
    }

    public static void main(String[] args) {
        Order ord1=new Order(22,"lcl");
        Order ord2=new Order(22,"lcl");
        Order ord3=new Order(23,"lzl");
        System.out.println(ord1.equals(ord2));
        System.out.println(ord1.equals(ord3));
    }
}
