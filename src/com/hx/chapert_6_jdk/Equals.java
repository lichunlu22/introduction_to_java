package com.hx.chapert_6_jdk;

import java.util.Objects;

public class Equals {

    private int x;
    private String y;

    public Equals() {
    }

    public Equals(int x, String y) {
        this.x = x;
        this.y = y;
    }

    public Equals(String y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public static void main(String[] args) {
        Equals s1=new Equals();
        s1.setX(5);
        s1.setY("lcl");
        //Equals s2=s1;
        Equals s2=new Equals();
        s2.setX(6);
        s2.setY("lzl");
        System.out.println(s1==s2);
        System.out.println(s1.equals(s2));
        System.out.println(s1.equals(s2));
        System.out.println("======================================");
        Equals C1=new Equals(9,"sss");
        Equals C2=new Equals(10,"lll");
        Equals c4=new Equals("78cl");
        Equals C3=C2;
        System.out.println(C1.hashCode());
        System.out.println(C2.hashCode());
        System.out.println(C3.hashCode());
        System.out.println(c4.hashCode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Equals equals = (Equals) o;
        return this.x == equals.x &&y.equals( equals.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
