package com.hx.chapert_6_jdk.clone;

public class Clone implements Cloneable{

    private String name;
    private int age;

    public Clone(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public static void main(String[] args) {
        try {
            Clone s1 = new Clone("Alice", 3);
            Clone duoli = (Clone) s1.clone();//克隆以后重新分配了地址
            System.out.println("s1==duoli:" + (s1 == duoli));//比较地址 false
            System.out.println(duoli.name + " " + duoli.age);//地址不同 内容相同
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }
}