package com.hx.chapert_6_jdk.jjdk;

public class jjdk {
    public static void main(String[] args) {
        String s="abcd-ef544-b4d";
        System.out.println(s.endsWith("a"));//判断是不是以指定的字符结尾
        System.out.println(s.indexOf("e"));//判断字符所在位置（从前往后）
        System.out.println(s.lastIndexOf("b"));//最后一次出现的位置（从后往前数）
        System.out.println(s.length());//判断字符串长度
        System.out.println(s.replace("544","666"));//替换字符串

        String ss[]=s.split("-");
        for (String i:ss){
            System.out.print(i+"  ");
        }

        System.out.println();

        System.out.println(s.substring(0));//从下标开始
        System.out.println(s.substring(6,8));//截取字符串

    }
}
