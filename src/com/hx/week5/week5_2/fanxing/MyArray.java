package com.hx.week5.week5_2.fanxing;

import java.util.ArrayList;
import java.util.List;

public class MyArray<T> {

    private T[] arr;
    private int i=0;

    public MyArray(){
        this(10);
    }

    public MyArray(int size){
        arr=(T[])new Object[size];
    }

    public void add(T t){
        arr[i]=t;
        i++;
    }
    public T get(int index){
        return arr[index];
    }

    public <E> List <E> fromArrayToList(E[] array){
        List<E> list=new ArrayList<>();
        for (E e:array){
            list.add(e);
        }
        return list;
    }
}
