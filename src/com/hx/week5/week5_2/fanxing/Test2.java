package com.hx.week5.week5_2.fanxing;

import com.hx.week5.week5_2.Users;

import java.util.ArrayList;
import java.util.List;

public class Test2 {
    public static void main(String[] args) {

        List<Users> users=new ArrayList<>();
        Users u1=new Users("lcl6",25);
        Users u2=new Users("lcl2",29);
        Users u5=new Users("lcl4",19);
        Users u6=new Users("lcl1",26);
        users.add(u1);users.add(u2);users.add(u5);users.add(u6);

        List<?> list=users;
        Users uu=(Users) list.get(2);
        System.out.println(uu);


        List<Integer> integers=new ArrayList<>();
        integers.add(5);integers.add(6);integers.add(7);
        System.out.println(sum(integers));

//        List<Object> strings=new ArrayList<>();
//        strings.add("55");strings.add("44");
//        System.out.println(sum(strings));//错误  不兼容

    }

    public static Double sum(List<? extends Number> list){
        Double s=0.0;
        for (Number N:list){
            s=s+N.doubleValue();
        }
        return s;
    }
}
