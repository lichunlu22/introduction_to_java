package com.hx.week5.week5_2;

import com.hx.collection.User;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        List<Users> users=new ArrayList<>();
        users.add(new Users("lcl",20));
        Users uu=new Users("lhz",23);
        users.add(uu);

        for (Users us:users){
            System.out.println(us);
        }

        System.out.println("==========================");

        Users uu2=new Users("lhy",18);
        users.add(uu2);
        System.out.println(users.contains(uu2));
        System.out.println(users.indexOf(uu2));

    }
}
