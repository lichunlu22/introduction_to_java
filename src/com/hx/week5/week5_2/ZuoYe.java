package com.hx.week5.week5_2;

import com.hx.week5.week5_2.fanxing.MyArray;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ZuoYe {
    public static void main(String[] args) {

        Users us1=new Users("lcl",23);
        Users us2=new Users("ww",25);
        Users us3=new Users("wang",22);
        Users us4=new Users("tang",22);
        Users us5=new Users("lang",22);
        Users us6=new Users("liu",22);

        List<Users> uss=new ArrayList<>();
        uss.add(us1);
        uss.add(us2);
        uss.add(us3);

        List<Users> uss2=new ArrayList<>();
        uss2.add(us4);
        uss2.add(us3);
        uss2.add(us6);

        System.out.println("===========================");
        MyArray<Users> uuu=new MyArray<>();
        uuu.add(us5);
        uuu.add(us6);

        String[] str={"666","999","555"};

        List list=uuu.fromArrayToList(str);

        Users[] uarr={us3,us4,us5};

        for (int i=0;i<3;i++){
            System.out.println(list.get(i));
        }
        for (int i=0;i<2;i++){
            System.out.println(uuu.get(i));
        }



        System.out.println("============================");

        Map<String,Object> map1=new LinkedHashMap<>();
        map1.put("name","学士");
        map1.put("size",uss.size());
        map1.put("list",uss);

        Map<String,Object> map2=new LinkedHashMap<>();
        map2.put("name","硕士");
        map2.put("size",uss2.size());
        map2.put("list",uss2);

        List<Map<String,Object>> usAll=new ArrayList<>();
        usAll.add(map1);
        usAll.add(map2);

       // Set<String> keys=map1.keySet();//取出所有的key值
       // Collection<Object> value=map1.values();//取出所有的value值

        for (Map<String,Object> map:usAll){
            //System.out.println(map);
            for (Map.Entry set:map.entrySet()){
                //System.out.println(set);
                //System.out.println(set.getKey()+":"+set.getValue());
                Object value=set.getValue();
                System.out.println(set.getKey()+":"+value);
                if (value instanceof List){

                }
            }
       }
    }
}
