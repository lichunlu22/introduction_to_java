package com.hx.week5.week5_2.test;

import java.util.*;

public class MapDemo {
    public static void main(String[] args) {
        Map<String,Object> map=new LinkedHashMap<>();
        map.put("name","张三");
        map.put("chengji",90);

        map.put("name","李四");
        map.put("chengji",96);

        Map<String,Object> map2=new LinkedHashMap<>();
        map2.put("name","张四");
        map2.put("chengji",96);

        Map<String,Object> map3=new LinkedHashMap<>();
        map3.put("name","张五");
        map3.put("chengji",95);

        Map<String,Object> map4=new LinkedHashMap<>();
        map4.put("name","张六");
        map4.put("chengji",89);

//        int chengji=(Integer) map.get("chengji");
//        System.out.println(chengji);获取第一个map的成绩

        List<Map<String,Object>> all=new ArrayList<>();
        all.add(map);
        all.add(map2);
        all.add(map3);
        all.add(map4);

        for (Map<String,Object> mmp:all){
            System.out.println(mmp);
        }

    }
}
