package com.hx.week5.week5_2.test;

import java.util.*;

public class SortDemo {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        List<Integer> list=new ArrayList<>();
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());
        list.add(sc.nextInt());

        System.out.print(list+" "+"\n");

        //倒序
        Collections.reverse(list);
        for (int i=0;i<list.size();i++){
            System.out.print(list.get(i)+" ");
        }

        System.out.println();
        System.out.println("======================");

        //从大到小排序
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2-o1;
            }
        });

        for (Integer in:list){
            System.out.print(in+" ");
        }
    }
}
