package com.hx.week5.week5_2;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class SetDemo {
    public static void main(String[] args) {
        System.out.println("===========HashSet===============");
        Set<Users> users=new HashSet<>();
        Users u1=new Users("lcl6",25);
        Users u2=new Users("lcl2",29);
        Users u5=new Users("lcl4",19);
        Users u6=new Users("lcl1",26);
        Users u7=new Users("lcl1",26);
        users.add(u1);
        users.add(u2);
        users.add(u5);
        users.add(u6);
        users.add(u7);
       for (Users u:users){
           System.out.println(u);
       }
        System.out.println("=========== LinkedHashSet============");
        Set<Users> users1=new LinkedHashSet<>();
        Users u11=new Users("lcl6",25);
        Users u22=new Users("lcl2",29);
        Users u55=new Users("lcl4",19);
        Users u66=new Users("lcl1",26);
        Users u77=new Users("lcl1",26);
        users1.add(u11);
        users1.add(u22);
        users1.add(u55);
        users1.add(u66);
        users1.add(u77);
        for (Users user:users1){
            System.out.println(user);
        }




    }
}
