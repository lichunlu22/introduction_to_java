package com.hx.week5.week5_3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class EcmDef {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);
        try {
            int a,b,c;
            a=scanner.nextInt();
            b=scanner.nextInt();
            c=a/b;
            System.out.println(c);
        } catch (ArithmeticException e) {
            //e.printStackTrace();
            System.out.println("除数不能为0");
        }catch (InputMismatchException e){
            System.out.println("数据类型不一致");
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("缺少参数");
        }
        System.out.println("程序结束");
    }

    public static int chufa(int a,int b){
        if(a<0||b<0)
            throw new EcDef("不能为负数");
        return a/b;
    }
}
