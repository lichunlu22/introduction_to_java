package com.hx.week5.week5_3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Exception11 {
    public static void main(String[] args) {
        double a,b,c;
        System.out.println("请输入两个整数：");
        Scanner scanner=new Scanner(System.in);
        try {
            a=scanner.nextInt();
            b=scanner.nextInt();
            c=a/b;
            if (String.valueOf(c).equals("Infinity")){
                throw new ArithmeticException();//double类型需要手动抛出异常throw
            }
            System.out.println("商为："+c);
        } catch (InputMismatchException e) {
            //e.printStackTrace();
            System.out.println("输入不合法");
        }catch (ArithmeticException e){
            //e.printStackTrace();
            System.out.println("除数不能为0");
            return;//加了return,后面的程序不执行，除非放在finally里面，才可以继续执行
        }finally {
            System.out.println("程序结束！");
        }


//        try {
//            readlcl();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }//调用方法的时候处理异常
    }


    public static void readlcl() throws FileNotFoundException {//把异常放到这里声明异常
        File file=new File("d:\\xxx.doc");
        FileInputStream filed=new FileInputStream(file);//有异常
    }


}
