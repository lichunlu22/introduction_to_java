package com.hx.week5.week5_4;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Recursion {
    public static void main(String[] args) {
        File file=new File("d:");
        open(file);

    }

    public static void open(File file) {

        List<File> fileList = new ArrayList<File>();
        File[] files = file.listFiles();
        // 如果目录为空，直接退出
        if (files == null) {
            return;
        }

        // foreach遍历files数组
        for (File a : files) {
            if (a.isFile()) {
                fileList.add(a);// 是文件，则添加到List<File>中
            } else if (a.isDirectory()) {
                open(a);
            }
        }

        for (File f2 : fileList) {
            System.out.println(f2.getName());
        }

    }
}
