package com.hx.week5.week5_4;

import java.io.*;

public class IoDemo {
    public static void main(String[] args) {
        //1.输入，输出，文件
//        try {
//            FileInputStream fis=new FileInputStream("f:lcl.txt");
//            FileOutputStream fos=new FileOutputStream("f:lcl1.txt");
//            int a;
//            while ((a=fis.read())!=-1){
//                fos.write(a);
//                System.out.print((char)a);
//            }
//            fos.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        //2.
//        long startTime = System.currentTimeMillis();    //获取开始时间
//
//        try {
//            FileInputStream fis1=new FileInputStream("f:计算圆柱体积.ASF");
//            FileOutputStream fos2=new FileOutputStream("f:计算圆柱体积lc2.ASF");
//            int a;
//            while ((a=fis1.read())!=-1){
//                fos2.write(a);
//            }
//            fos2.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        long endTime = System.currentTimeMillis(); //获取结束时间
//
//        System.out.println("程序运行时间：" + (endTime - startTime)/1000/60 + "min");    //输出程序运行时间


        //3.byte

//        try {
//            FileInputStream fis1=new FileInputStream("f:计算圆柱体积.ASF");
//            FileOutputStream fos2=new FileOutputStream("f:计算圆柱体积lc33.ASF");
//            byte[] tmp=new byte[8192];
//            int len;
//
//            long startTime = System.currentTimeMillis();    //获取开始时间
//            while ((len=fis1.read(tmp))!=-1){
//                fos2.write(tmp,0,len);
//            }
//            fos2.close();fis1.close();
//            long endTime = System.currentTimeMillis(); //获取结束时间
//            System.out.println("程序运行时间：" + (endTime - startTime)/1000 + "s");    //输出程序运行时间
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        //4.Buffer
        //用Buffer把File包装一下
        long startTime = System.currentTimeMillis();    //获取开始时间

        try {
            FileInputStream fis1=new FileInputStream("f:计算圆柱体积.ASF");
            FileOutputStream fos2=new FileOutputStream("f:计算圆柱体积lc2222222222.ASF");
            BufferedInputStream bis=new BufferedInputStream(fis1);
            BufferedOutputStream bos=new BufferedOutputStream(fos2);
            int a;
            while ((a=bis.read())!=-1){
                bos.write(a);
            }
            bis.close();bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis(); //获取结束时间

        System.out.println("程序运行时间：" + (endTime - startTime)/1000 + "s");    //输出程序运行时间

    }
}
