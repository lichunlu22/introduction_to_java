package com.hx.week5.week5_4;

import java.io.File;
import java.io.IOException;

public class JavaIo {
    public static void main(String[] args) {
        File file=new File("e:\\myjava\\Hello.java");
        System.out.println(file.length());//输出调用文件的大小111

        System.out.println(file.isFile());//判断是否为文件true

        System.out.println(file.exists());//判断文件是否存在true

        System.out.println(file.isDirectory());//判断是否为目录false

        String a=file.getName();
        System.out.println(a);//获得文件名称

        File aa=new File("e:/myjava");
        File[] bb=aa.listFiles();//返回文件夹内的子文件与子文件夹的数组
        for (File cc:bb) {
            //System.out.println(cc);获取目录全部信息
            System.out.println(cc.getName());//获取目录中的文件名字
        }

        System.out.println("==============创建文件===============");
        File file1=new File("e:\\myjava\\lcl.txt");
        if (!file1.exists()){//判断文件是否存在，不存在就创建
            try {
                file1.createNewFile();//创建文件
                System.out.println("文件创建成功");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //===================删除文件===============================
        System.out.println(file1.delete());

     //=======================创建目录=============================
        File file2=new File("f:\\LCL");
        if (!file2.exists()){//判断F盘中有没有LCL这个目录，没有就创建为目录
            file2.mkdir();
        }

        File lcl1=new File("f:/LCL/lcl1");//创建lcl1为LCL的子目录
        if (!lcl1.exists()){
            lcl1.mkdir();
        }

        File lcl4 = new File("f:\\LCL", "lcl3/lcl4");//同时创建lcl3和lcl4,lcl4是lcl3的子目录
        if (!lcl4.exists()) {
            lcl4.mkdirs();
        }

        //=====================删除目录===========================

        File file3=new File("f:/LCL/lcl3");
        System.out.println(file3.delete());//删除目录，只能删除空目录!!!
//        boolean b=file3.delete();
//        System.out.println(b);

        File[] roots=File.listRoots();
        for (File f:roots){
            System.out.println(f.getPath());
        }
        File[] dfile=roots[0].listFiles();
        for (File f:dfile){
            System.out.println(f.getPath());
        }
    }
}
