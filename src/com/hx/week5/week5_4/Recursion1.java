package com.hx.week5.week5_4;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Recursion1 {
    public static void main(String[] args) {
        File file=new File("d:");
        dakai(file);

    }



    public static void dakai(File file){
        List<File> files=new ArrayList<>();//存文件名
        File[] aa=file.listFiles();//返回文件夹内的子文件与子文件夹的数组

        //先判断不是空的在继续往下执行
        if (aa==null){
            return;
        }

        for (File bb:aa){
            if (bb.isFile()){
                files.add(bb);//判断是文件则添加到files中
            }else if (bb.isDirectory()){
                dakai(bb);//判断是目录则调用本身继续判断（递归）
            }
        }


        for (File file1:files){//打印files中存的所有文件名
            System.out.println(file1.getName());
        }
//        for (File file1:aa)
//            System.out.println(file.getName());
    }
}
