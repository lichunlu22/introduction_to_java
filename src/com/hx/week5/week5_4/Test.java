package com.hx.week5.week5_4;

import java.io.*;

public class Test {
    public static void main(String[] args) throws IOException {
        File file = new File("input.txt");   //创建文件对象
        if (!file.exists())                  //判断该文件是否存在，如果不存在则创建新文件
        {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileWriter fr = null;   //针对文件对象创建文件写入流对象
        try {
            fr = new FileWriter(file);
        } catch (IOException e) {


        }
        BufferedWriter bw = new BufferedWriter(fr);   //为文件写入流建立缓冲流
//将控制台输入对象转化成字符流，并建立缓冲流
        BufferedReader bin = new BufferedReader(new InputStreamReader(System.in));
        String str = null;    //接受从控制台输入的一行字符串
        try {
            str = bin.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (!(str.equals("!!!")))    //如果输入"!!!"则代表输入结束
        {
            try {
                bw.write(str);              //将从控制台输入的字符串写入到文件中
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bw.newLine();               //换新行
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                str = bin.readLine();       //再从控制台接受输入
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//关闭所有已经打开的流
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fr.close();
        bin.close();

    }
}
