package com.hx.week5.week5_4;

import java.io.File;

public class Recursion2 {
    public static void main(String[] args) {
        File file=new File("d:");
        open(file);
    }

    private static void open(File file) {
        File[] files=file.listFiles();//打开文件中的目录和文件
        if (files==null) return;//判断目录是不是空
        for (File file1:files){
            System.out.println(file1.getPath());//输出文件绝对路径
            if (file1.isDirectory()){
                open(file1);
            }
        }
    }
}
