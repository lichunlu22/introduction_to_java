package com.hx.week5.week5_4;

import java.io.File;

public class ListFile {
    private static int level;

    public static void main(String[] args) {
        File file=new File("f:");
        showFile(file);
    }

    private static void showFile(File file) {
        level++;
        File[] files=file.listFiles();
        if (files==null)return;
        for (File f:files){
            String spacee="";
            for (int i=1;i<level;i++)
                spacee+="   ";
            System.out.println(spacee+f.getName());
            if (f.isDirectory())
                showFile(f);
        }
        level--;
    }
}
