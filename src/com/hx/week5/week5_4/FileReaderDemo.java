package com.hx.week5.week5_4;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderDemo {
    public static void main(String[] args) {
//        try {
//            FileReader fr=new FileReader("doc/b.txt");
//            FileWriter fw=new FileWriter("doc/c.txt");
//            int a;
//            while ((a=fr.read())!=-1){
//                fw.write(a);
//                System.out.print((char)a);
//            }
//
//            fr.close();fw.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        //加密
        try {
            FileReader fr=new FileReader("doc/b.txt");
           // FileWriter fw=new FileWriter("doc/b.txt");
            char[] chars=new char[10000];
            int len;
            int key=527;
            len=fr.read(chars);
            fr.close();
            FileWriter fw=new FileWriter("doc/b.txt");


            for (int i=0;i<len;i++){
                fw.write(chars[i]^key);
            }
            fw.flush();
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
