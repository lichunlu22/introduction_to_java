package com.hx.week5.week5_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemInDemo {
    public static void main(String[] args) {
        //转换System.in
        InputStreamReader isr=new InputStreamReader(System.in);
        //这样的字符流效率很低，再使用BufferedReader类来为其建立缓冲，如：
        BufferedReader br=new BufferedReader(isr);

        //这样，就可以从控制台接受输入了。
        try {
            int a;
            while ((a=br.read())!=-1)
            System.out.println((char) a);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
