package com.hx.week5.week5_5;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class SystemOutDemo {
    public static void main(String[] args) {
        try {
            PrintStream ps=new PrintStream("doc/c.txt");
            PrintStream sout=System.out;//存储
            System.setOut(ps);//通过System类的setIn，setOut方法对默认设备进行改变。
            System.out.println("555555555555555555555555");//输出到文件
            System.out.println("7777");//输出到文件
            ps.close();//关闭流
            System.setOut(sout);//更改为默认的设备（调用）
            System.out.println("========================");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
