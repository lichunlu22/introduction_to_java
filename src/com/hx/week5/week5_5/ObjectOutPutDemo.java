package com.hx.week5.week5_5;

import com.hx.week5.week5_2.Users;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectOutPutDemo {
    public static void main(String[] args) {
        //Users users=new Users("孙悟空",1000);
        try {
           /* ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("aa.txt"));
            oos.writeObject(users);
            oos.close();*/

           ObjectInputStream ois=new ObjectInputStream(new FileInputStream("aa.txt"));
           Users users1=(Users) ois.readObject();
            System.out.println(users1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
