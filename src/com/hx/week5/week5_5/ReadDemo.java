package com.hx.week5.week5_5;

import java.io.*;

public class ReadDemo {
    public static void main(String[] args) {
        File file=new File("doc/c.txt");
        try {
            //通过文件对象创建文件读取流对象
            FileReader fr = new FileReader(file);

            //将文件读取流包装成缓冲读取流
            BufferedReader br=new BufferedReader(fr);

            String str;
            while ((str=br.readLine())!=null){//逐行读取数据readLine(字符串才可以逐行读取)
                System.out.println(str);
            }
            br.close();//关闭流
            fr.close();//关闭流
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
