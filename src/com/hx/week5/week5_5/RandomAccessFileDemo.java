package com.hx.week5.week5_5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessFileDemo {
    public static void main(String[] args) {
        File file=new File("doc/aa.txt");
        try {
            RandomAccessFile raf=new RandomAccessFile(file,"rw");
            raf.seek(5);
            byte[] b=new byte[1024];

            int off=0;
            int len=5;
            raf.read(b,off,len);//从这个文件读取 len字节的数据到一个字节数组。
            String str=new String(b,0,len);
            System.out.println(str);
            raf.close();

            RandomAccessFile ss=new RandomAccessFile("aa.txt","rw");

            raf.seek(5);
            String temp=raf.readLine();
            System.out.println(temp);

            raf.seek(5);
            raf.write("xyz".getBytes());
            raf.write(temp.getBytes());

            raf.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
