package com.hx.week5.week5_5;

import java.io.RandomAccessFile;

public class RandomAccessFileDemo2 {
    public static void main(String[] args) {
        try{
            //RandomAccessFile raf=new RandomAccessFile(new File("D:\\3\\test.txt"), "r");

//            RandomAccessFile raf=new RandomAccessFile("doc/raf.txt", "r");
//            //获取RandomAccessFile对象文件指针的位置，初始位置是0
//            System.out.println("RandomAccessFile文件指针的初始位置:"+raf.getFilePointer());
//            raf.seek(5);//移动文件指针位置
//            byte[]  buff=new byte[1024];
//            //用于保存实际读取的字节数
//            int hasRead=0;
//            //循环读取
//            while((hasRead=raf.read(buff))>0){
//                //打印读取的内容,并将字节转为字符串输入
//                System.out.println(new String(buff,0,hasRead));
//
//            }

            RandomAccessFile raf=new RandomAccessFile("doc/raf.txt","rw");
            raf.seek((raf.length())/2);
            raf.write("55555555555555555555555555555555555555".getBytes());



        }catch(Exception e){
            e.printStackTrace();
        }

    }
}
