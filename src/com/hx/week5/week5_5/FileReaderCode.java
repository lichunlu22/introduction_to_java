package com.hx.week5.week5_5;

import java.io.*;

public class FileReaderCode {
    public static void main(String[] args) {
        try {
//            FileReader fileReader=new FileReader("doc/c.txt");
//            int a=fileReader.read();
//            System.out.println((char)a);
            //将文件编码改为GBK,默认编码为UTF-8,读取为乱码，必须设置需要读取的编码
            InputStreamReader isr=new InputStreamReader(new FileInputStream("doc/c.txt"),"GBK");
            int a;
            while ((a=isr.read())!=-1) {
                System.out.print((char) a);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
