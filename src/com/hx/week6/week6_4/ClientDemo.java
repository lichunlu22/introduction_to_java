package com.hx.week6.week6_4;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientDemo {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("192.168.102.96", 8080);
            new Receive(socket).start();
            System.out.println("客户端启动");

            BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));//输出

            Scanner sc=new Scanner(System.in);
            while (true){
                //输出数据
                String s=sc.nextLine();
                bw.write(s+"\n");
                bw.flush();//刷新
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
