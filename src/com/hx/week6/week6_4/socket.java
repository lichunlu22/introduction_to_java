package com.hx.week6.week6_4;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class socket {
    public static void main(String[] args) {
        try {
            ServerSocket server=new ServerSocket(8080);
            System.out.println("服务器启动");

            while (true) {
                int i=0;
                i++;
                Socket socket=server.accept();//等待客户端连接
                System.out.println("接受到客户端"+i+"的连接："+socket.getPort());

                List list=new LinkedList();
                list.add(socket);

                for (Object object:list){
                    System.out.println(object);
                }

                new Receive(socket).start();
                BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                Scanner sc=new Scanner(System.in);
                while (true){
                    //输出数据
                    String s=sc.nextLine();
                    bw.write(s+"\n");
                    bw.flush();//刷新
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
