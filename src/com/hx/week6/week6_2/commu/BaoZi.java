package com.hx.week6.week6_2.commu;

public class BaoZi {
    private Integer id;
    private  String name;

    public BaoZi() {
    }

    public BaoZi(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BaoZi{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
