package com.hx.week6.week6_2.commu;

import java.util.Random;

public class Customer extends Thread {
    private Container<BaoZi> container;

    private Integer sum;

    public Customer(Container<BaoZi> container, Integer sum,String name){
        super(name);
        this.container=container;
        this.sum=sum;
    }

    @Override
    public void run() {
        Random random=new Random();
        for (int i=0;i<sum;i++){
            BaoZi baoZi=container.fatch();
            System.out.println(Thread.currentThread().getName()+"买了一个包子"+baoZi);
            try {
                Thread.sleep(2000+random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
