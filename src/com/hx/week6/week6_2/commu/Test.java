package com.hx.week6.week6_2.commu;

public class Test {
    public static void main(String[] args) {
        Container<BaoZi> container=new Container();

        Producer producer=new Producer(container,20,"我");

        Customer customer1=new Customer(container,3,"甲");
        Customer customer2=new Customer(container,5,"乙");
        Customer customer3=new Customer(container,8,"丙");
        Customer customer4=new Customer(container,4,"丁");

        producer.start();customer1.start();customer2.start();customer3.start();customer4.start();

    }
}
