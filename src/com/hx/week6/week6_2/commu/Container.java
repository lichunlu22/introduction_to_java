package com.hx.week6.week6_2.commu;

public class Container<T> {
    private T[] arr;

    private int top = 0;

    public Container() {
        this(5);
    }

    public Container(int size) {
        this.arr = (T[]) new Object[size];
    }

    public synchronized void save(T o) {

        while (top == arr.length) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
            arr[top] = o;
            top++;
           // System.out.println("生产了第"+top+"个"+"包子");
            notify();
    }

    public synchronized T fatch() {
        while (top == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
       // System.out.println("消费了第"+top+"个包子");
        top--;
        T t=arr[top];
        notify();
        return t;
    }
    public int count(){
        return top;
    }
}
