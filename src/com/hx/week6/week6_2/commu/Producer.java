package com.hx.week6.week6_2.commu;

import java.util.Random;

public class Producer extends Thread{
    private Container<BaoZi> container;

    private Integer sum;

    public Producer(Container<BaoZi> container,Integer sum,String name){
        super(name);
        this.container=container;
        this.sum=sum;
    }

    @Override
    public void run() {
        String [] name={"鲜肉包","香菇包","豆沙包"};
        Random random=new Random();
        for (int i=0;i<sum;i++){
            BaoZi baoZi=new BaoZi(i+1,name[random.nextInt(3)]);
            System.out.println(Thread.currentThread().getName()+"做l一个包子"+baoZi);
            container.save(baoZi);
            try {
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
