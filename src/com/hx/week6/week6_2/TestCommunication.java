package com.hx.week6.week6_2;

public class TestCommunication {
    public static void main(String[] args) {

        PrintNum pn=new PrintNum();
        Thread td1=new Thread(pn);
        Thread td2=new Thread(pn);

        td1.setName("甲");
        td2.setName("乙");

        td1.start();
        td2.start();

    }
}
class PrintNum implements Runnable{
    int num=1;
    @Override
    public void run() {
        while (true){
            synchronized (this) {
                notify();
                if (num<=100){
                    try {
                        Thread.currentThread().sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName()+":"+num);
                    num++;
                }else {
                    break;
                }
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}