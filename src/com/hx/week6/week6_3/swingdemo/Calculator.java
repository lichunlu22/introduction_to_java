package com.hx.week6.week6_3.swingdemo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Calculator extends JFrame implements ActionListener {
    private JTextField textField;
    public Calculator(){
        setTitle("计算器");//
        setSize(300,400);//设置窗口大小
        setVisible(true);//设置窗口可见
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//设置退出（不然退出窗口以后，程序还在运行）
        JPanel panelNorth=new JPanel();//创建上半部分的容器
        JPanel panelConter=new JPanel();//下半部分的容器
        add(panelNorth, BorderLayout.NORTH);//添加到上方（北方）
        add(panelConter);//添加到中间(默认是在中间)
        //北面板
        textField=new JTextField();//创建输入框
        JButton clrButton=new JButton("清除");//创建清除按钮
        clrButton.addActionListener(this);
        panelNorth.setLayout(new BorderLayout());//
        panelNorth.add(textField);
        panelNorth.add(clrButton,BorderLayout.EAST);
        //中面板
        String []btnStr={"7","8","9","+","4","5","6","-","1","2","3","*","0",".","=","/" };
        panelConter.setLayout(new GridLayout(4,4));
        for (String s:btnStr){
            JButton btn=new JButton(s);
            btn.addActionListener(this);
            panelConter.add(btn);
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String txt=e.getActionCommand();
        if ("0123456789+-*/ ".indexOf(txt)>=0){
            textField.setText(textField.getText()+txt);
        }else if (txt.equals("=")) {
            //执行计算
            Map<String,Integer> pri=new HashMap<>();
            pri.put("-",1);
            pri.put("+",1);
            pri.put("*",2);
            pri.put("/",2);

            String exp = textField.getText();
            String[] numbers = exp.split("[\\+\\-\\*\\/]");//把数字切出来
            String[] ops = exp.split("[0-9]+");//把符号切出来
            ops= Arrays.copyOfRange(ops,1,ops.length);

            //建栈
            Stack<Integer> stack1=new Stack<>();
            Stack<String> stack2=new Stack<>();
            int len=numbers.length;
            stack1.push(Integer.valueOf(numbers[len-1]));
            String a=null;
            for (int i=len-2;i>=0;i--) {
                if (!stack2.isEmpty())
                    a = stack2.peek();
                if (a == null || pri.get(a) < pri.get(ops[i])) {
                    stack2.push(ops[i]);
                    stack1.push(Integer.valueOf(numbers[i]));
                } else {
                    int z = getZ(stack1, stack2);
                    stack1.push(z);
                    stack1.push(Integer.valueOf(numbers[i]));
                    stack2.push(ops[i]);
                }
            }

            while (!stack2.isEmpty()){
                int z = getZ(stack1, stack2);
                stack1.push(z);
            }

            textField.setText(textField.getText()+"="+stack1.pop());//把文件结果输出到
            //System.out.println(stack1.pop());
        }
    }

    public int getZ(Stack<Integer> stack1, Stack<String> stack2) {
        String t = stack2.pop();
        Integer x = stack1.pop();
        Integer y = stack1.pop();
        int z = 0;
        switch (t) {
            case "+":
                z = x + y;
                break;
            case "-":
                z = x - y;
                break;
            case "*":
                z = x * y;
                break;
            case "/":
                z = x / y;
                break;
        }
        return z;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Calculator();
            }
        });
    }
}
