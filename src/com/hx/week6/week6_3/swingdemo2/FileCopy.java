package com.hx.week6.week6_3.swingdemo2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class FileCopy extends JFrame implements ActionListener {
    private JButton button1;
    private JButton button2;
    private JTextField textField1;
    private JTextField textField2;
    private JButton copyBtn;

    public FileCopy() {
        setTitle("文件复制");
        setSize(600, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(null);

        JLabel label1 = new JLabel("源文件");
        textField1 = new JTextField();
        button1 = new JButton("...");
        button1.addActionListener(this);
        label1.setBounds(50, 50, 50, 25);
        add(label1);
        textField1.setBounds(110, 50, 380, 25);
        add(textField1);
        button1.setBounds(500, 50, 50, 25);
        add(button1);

        JLabel label2 = new JLabel("目的地");
        textField2 = new JTextField();
        button2 = new JButton("...");
        button2.addActionListener(this);
        label2.setBounds(50, 100, 50, 25);
        add(label2);
        textField2.setBounds(110, 100, 380, 25);
        add(textField2);
        button2.setBounds(500, 100, 50, 25);
        add(button2);

        copyBtn = new JButton("复制");
        copyBtn.setBounds(240, 150, 120, 30);
        add(copyBtn);
        copyBtn.addActionListener(this);

        JProgressBar progressBar = new JProgressBar();
        progressBar.setBounds(50, 240, 500, 30);
        add(progressBar);
        progressBar.setStringPainted(true);
        //progressBar.setValue(10);//设置开始进度
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FileCopy();
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o == button1) {//点击选择文件
            JFileChooser fileChooser = new JFileChooser();
            int f = fileChooser.showOpenDialog(this);
            if (f == JFileChooser.APPROVE_OPTION) {
                textField1.setText(fileChooser.getSelectedFile().getPath());
                /*System.out.println(fileChooser.getSelectedFile().getPath());*/
            }
        } else if (o == button2) {//点击选择目标文件
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int f = fileChooser.showOpenDialog(this);
            if (f == JFileChooser.APPROVE_OPTION) {
                textField2.setText(fileChooser.getSelectedFile().getPath());
            }
        } else if (o == copyBtn) {//点击复制
            String srcPath=textField1.getText(),destPath=textField2.getText();
            File srcFile=new File(srcPath);
            File destFile=new File(destPath+"/"+srcFile.getName());
            copy(srcFile,destFile);


        }
    }

    private void copy(File srcFile, File destFile) {
        try {
            BufferedInputStream bis=new BufferedInputStream(new FileInputStream(srcFile));
            BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream(destFile));
            byte[]tmp=new byte[8192];
            int len;
            while ((len=bis.read(tmp))!=-1){
                bos.write(tmp,0,len);
            }
            bos.close();bis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}