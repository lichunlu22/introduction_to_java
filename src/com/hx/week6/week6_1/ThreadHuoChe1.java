package com.hx.week6.week6_1;

public class ThreadHuoChe1 implements Runnable{
    int tickets=100;
    int i=1;

    @Override
    public void run() {
        while(true){
            if (tickets>0) {
                System.out.println(i+":"+Thread.currentThread().getName()+"正在售票...票号为："+
                        tickets--);
            }
            else{
                break;
            }
            i++;
        }

    }

    public static void main(String[] args) {
        ThreadHuoChe1 tw=new ThreadHuoChe1();
        Thread one=new Thread(tw);
        Thread two=new Thread(tw);
        Thread three=new Thread(tw);
        one.start();
        two.start();
        three.start();

    }
}
