package com.hx.week6.week6_1;

public class ThreadHuoChe1_1 {
    public static void main(String[] args) {
        //========================创建了一个对象，三个线程============================================
        Tickets ts=new Tickets();
        Thread td1=new Thread(ts,"一号窗口");
        Thread td2=new Thread(ts,"二号窗口");
        Thread td3=new Thread(ts,"三号窗口");
        td1.start();td2.start();td3.start();
    }


}

class Tickets implements Runnable{
    //============================所以这里不用加static也可以===========================================
    private int tickets=100;

    @Override
    public void run() {
        int c=0;
        while (tickets>0) {
            c++;
            System.out.println(Thread.currentThread().getName()+"卖第"+c+"张");
            tickets--;
        }
    }
}
