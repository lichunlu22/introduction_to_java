package com.hx.week6.week6_1;

public class ThreadDemo2 implements Runnable{

    private static int count;

    @Override
    public void run() {
        for (int i=1;i<100;i++){
            System.out.println(Thread.currentThread().getName()+"====>"+i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadDemo2 threadDemo2=new ThreadDemo2();
        Thread thread=new Thread(threadDemo2);
        thread.start();
        while(true){
            System.out.println("++++++++++++++++++++++++++");
            count++;
            if (count>20) thread.join();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
