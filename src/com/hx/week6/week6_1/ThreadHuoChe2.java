package com.hx.week6.week6_1;

public class ThreadHuoChe2 implements Runnable{
    private int tickets=100;
    @Override
    public void run() {
        int c=0;
        while (true){
            synchronized (this) {//锁住当前对象（this），包含着需要锁的代码块
                if (tickets<=0) break;
                tickets--;
            }
            c++;
            System.out.println(Thread.currentThread().getName()+"第"+c+"张票");//获得线程名称
        }
    }

    public static void main(String[] args) {
        ThreadHuoChe2 td=new ThreadHuoChe2();
        Thread td1=new Thread(td);
        Thread td2=new Thread(td);
        Thread td3=new Thread(td);
        td1.setName("一号窗口");
        td2.setName("二号窗口");
        td3.setName("三号窗口");
        td1.start();
        td2.start();
        td3.start();
    }
}
