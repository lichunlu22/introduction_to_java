package com.hx.week6.week6_1;

public class ThreadHuoChe extends Thread{
//========================加了static=====================================
    private static int piao=100;

    @Override
    public void run() {
       while (true){
            if (piao>0)
            System.out.println(Thread.currentThread().getName()+"正在售出第"+piao--+"张票");
        }
    }

    public static void main(String[] args) {
        //=======================创建了三个对象，所以要用static,分享一个变量=====================
        ThreadHuoChe one=new ThreadHuoChe();
        ThreadHuoChe two=new ThreadHuoChe();
        ThreadHuoChe three=new ThreadHuoChe();
        one.start();
        two.start();
        three.start();
    }
}
