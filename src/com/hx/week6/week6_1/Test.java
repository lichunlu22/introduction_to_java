package com.hx.week6.week6_1;

public class Test {
    public static void main(String[] args) {
        Thread th1=new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=1;i<10;i++){
                    System.out.println("qqq");
                }
            }
        });

        Thread th2=new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=1;i<10;i++){
                    System.out.println("vvv");
                }
            }
        });

        Thread th3=new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){//死循环
                    System.out.println(th1.getState()+" "+th2.getState());//输出状态
                }
            }
        });
        th3.setDaemon(true);//把th3设置成守护线程
        th1.start();th2.start();
        th3.start();//th1和th2结束,th3就会结束
    }
}
