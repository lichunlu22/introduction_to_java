package com.hx.week6.week6_1;

/***
 * 银行有一个账户。
 * 有两个储户分别向同一个账户存3000元，每次存1000，存3次。每次存完打印账户余额。
 */
public class BankAccount {
    public static void main(String[] args) {
        Account account=new Account();
        Thread td1=new Thread(account,"甲");
        Thread td2=new Thread(account,"乙");
        td1.start();
        td2.start();
    }
}

class Account implements Runnable{//账户
    private double money=0;

    @Override
    public void run() {
       for (int i=0;i<3;i++){
           synchronized (this) {
               money+=1000;//每次存1000元
               System.out.println(Thread.currentThread().getName()+"存入以后，余额为"+money);
           }
       }
    }
}













//class Customer extends Thread{//用户
//
//    public Customer(Account account){
//        this.
//    }
//    @Override
//    public void run() {
//        for (int i=0;i<3;i++){
//
//        }
//    }
//}