package com.hx.week6.week6_1;

public class ThreadTest implements Runnable{

    @Override
    public void run() {
        for (int i=1;i<100;i++){
            if (i%2==0){
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadTest t1=new ThreadTest();
        Thread th=new Thread(t1);
        th.start();
        //ThreadTest t2=new ThreadTest();

        for (int i=1;i<100;i++){
            th.join();
            if (i%2!=0){
                System.out.println(i);
            }
        }


    }
}
