package com.hx.ppt.t_5_24;

public class Single {
    //构造器 私有
    private Single(){
    }

    private static Single lcl=new Single();

    public static Single getSingle(){
        return lcl;
    }

}
